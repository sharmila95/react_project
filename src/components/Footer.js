import React from 'react'

function Footer() {
    return (
        <div className="container-fluid footer bg-light">
            <div className="container">
                <div className="col-md-12 p-0">
                    <p className="copyRight text-left">Copyright @ GoBus Service India Pvt. Ltd. All Right Reserved.</p>
                </div>
            </div>            
        </div>        
    )
}

export default Footer
