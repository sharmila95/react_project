import React from 'react'
import logo from '../imgs/logo1.PNG'
import '../../node_modules/font-awesome/css/font-awesome.min.css'

function Navbar() {
    const mystyle = {
        color: "#777777",
        height: "10px",
        width: "10px"
      };
    return (
        <div>
            <nav className="navbar navbar-expand-md navbar-light bg-light">
                <div className="container">
                <a href="#" className="navbar-brand">

                    <img src={logo} alt="GoBus travel your world with us" className="responsive"/>
                </a>
                <button type="button" className="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarCollapse">
                    <div className="navbar-nav ml-auto">
                        {/* <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Dropdown button
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div> */}
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModalCenter"><i className="fa fa-user-circle-o fa-1 pr-3" style={mystyle}></i> Login / Register</button>
                    </div>
                </div>
            </div>        
        </nav>
        {/* Modal Popup */}
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-4">.col-md-4</div>
                                    <div class="col-md-4 ml-auto">.col-md-4 .ml-auto</div>
                                </div>
                                <div class="row">
                                <div class="col-md-3 ml-auto">.col-md-3 .ml-auto</div>
                                <div class="col-md-2 ml-auto">.col-md-2 .ml-auto</div>
                            </div>
                            <div class="row">
                                 <div class="col-md-6 ml-auto">.col-md-6 .ml-auto</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    Level 1: .col-sm-9
                                    <div class="row">
                                        <div class="col-8 col-sm-6">
                                            Level 2: .col-8 .col-sm-6
                                        </div>
                                        <div class="col-4 col-sm-6">
                                            Level 2: .col-4 .col-sm-6
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
}

export default Navbar
