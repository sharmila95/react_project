import React from 'react'
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: 200,
    },
  },
}));

function Main() {
    const classes = useStyles();
    return (
        <div className="container-fluid">
            <div className="container">
                <div className="title">
                    <h1 className="text-center">Book Bus Tickets</h1>
                </div>
                <div className="searchfield">
                    <form className={classes.root} noValidate autoComplete="off">
                        <TextField id="standard-basic" label="Standard" />
                    </form>
                </div>
            </div>
        </div>        
    )
}

export default Main